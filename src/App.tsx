import React from "react";
import { Header } from "./components/Header";
import { Sidenav, BodyWraper } from "./components/Sidenav";
import { Main } from "./components/Main";

const App: React.FC = () => {
  return (
    <>
      <Header />
      <BodyWraper>
        <Sidenav />
        <Main />
      </BodyWraper>
    </>
  );
};

export default App;
