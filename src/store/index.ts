import create from "zustand";
import produce, { original } from "immer";
import { v4 as uuidv4 } from "uuid";
import api from "../services/api";

interface Journey {
  name: string;
  status: number;
  recipients: string;
  success: string;
  id: string;
}

interface State {
  isModalVisible: boolean;
  searchName: string;
  filterStatus: number;
  tableData: Journey[];
  filteredTableData: Journey[];
}

interface Store {
  state: State;
  actions: {
    toggleModal: () => void;
    addJourney: (name: string) => void;
    fetchJourneys: () => Promise<void>;
    reset: () => void;
    filterJorneys: ({
      filterStatus,
      searchName,
    }: {
      filterStatus?: number;
      searchName?: string;
    }) => void;
  };
}

export const useStore = create<Store>((set): Store => {
  const setState = (fn: ({ state }: { state: State }) => void) =>
    set(produce(fn));

  const initialState = {
    isModalVisible: false,
    searchName: "",
    filterStatus: 0,
    tableData: [],
    filteredTableData: [],
  };

  return {
    state: {
      ...initialState,
    },
    actions: {
      toggleModal: () =>
        setState(({ state }) => {
          state.isModalVisible = !state.isModalVisible;
        }),
      reset: () => set({ state: { ...initialState } }),
      addJourney: (name) =>
        setState(({ state }) => {
          if (
            !original(state.tableData)
              ?.map((e) => e.name)
              .includes(name) &&
            name
          ) {
            state.tableData.push({
              name,
              status: 1,
              recipients: "0",
              success: "0%",
              id: uuidv4(),
            });
            state.filteredTableData.push({
              name,
              status: 1,
              recipients: "0",
              success: "0%",
              id: uuidv4(),
            });
          }
        }),
      fetchJourneys: async () => {
        const res = await api("journey");
        setState(({ state }) => {
          state.tableData = res.data;
          state.filteredTableData = res.data;
        });
      },
      filterJorneys: ({
        filterStatus,
        searchName,
      }: {
        filterStatus?: number;
        searchName?: string;
      }) =>
        setState(({ state }) => {
          state.filterStatus = filterStatus ? filterStatus : 0;
          state.searchName = searchName ? searchName.toLowerCase() : "";
          state.filteredTableData = state.tableData.filter((e: Journey) =>
            // eslint-disable-next-line
            filterStatus != 0
            // eslint-disable-next-line
              ? (filterStatus && e.status == filterStatus) ||
                (searchName &&
                  e.name.toLowerCase().includes(searchName.toLowerCase())) ||
                (!filterStatus && !searchName && true)
              : true
          );
        }),
    },
  };
});
