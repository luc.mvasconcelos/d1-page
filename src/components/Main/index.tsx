import React, { useEffect } from "react";
import { useStore } from "../../store";
import {
  RadioGroup,
  RadioItem,
  Avatar,
  Table,
  BedIcon,
  CheckIcon,
  PaperPlaneIcon,
  PenIcon,
  PlayCircleIcon,
  ThIcon,
} from "d1-react-components";

const Main = () => {
  const { tableData, filterStatus, filteredTableData } = useStore(
    (store) => store.state
  );
  const { fetchJourneys, filterJorneys } = useStore((store) => store.actions);

  useEffect(() => {
    fetchJourneys();
    // eslint-disable-next-line
  }, []);

  const statusToIcon = [
    <>
      {" "}
      <ThIcon size={"16px"} color="#D190DD" />
      <span style={{ marginLeft: "10px", marginRight: "13px" }}>Todas</span>
    </>,
    <>
      {" "}
      <PaperPlaneIcon size={"16px"} color="#C1CA4F" />
      <span style={{ marginLeft: "10px", marginRight: "13px" }}>
        Enviando
      </span>{" "}
    </>,
    <>
      {" "}
      <PlayCircleIcon size={"16px"} color="#35C667" />
      <span style={{ marginLeft: "10px", marginRight: "13px" }}>Ativadas</span>
    </>,
    <>
      {" "}
      <PenIcon size={"16px"} color="#3FA8F4" />
      <span style={{ marginLeft: "10px", marginRight: "13px" }}>
        Configurando
      </span>{" "}
    </>,
    <>
      {" "}
      <BedIcon size={"16px"} color="#EBBD3E" />
      <span style={{ marginLeft: "10px", marginRight: "13px" }}>
        Ociosa
      </span>{" "}
    </>,
    <>
      {" "}
      <CheckIcon size={"16px"} color="#9FABD5" />
      <span style={{ marginLeft: "10px", marginRight: "13px" }}>Concluída</span>
    </>,
  ];

  return (
    <main style={{ marginTop: "123px", marginLeft: "76px", display: "block" }}>
      <div style={{ marginBottom: "34px" }}>
        <span style={{ fontSize: "16px", fontWeight: "bold" }}>Jornadas</span>
      </div>
      <div style={{ display: "flex" }}>
        <RadioGroup>
          <RadioItem
          // eslint-disable-next-line
            checked={filterStatus == 0}
            value={0}
            onChange={(e) => filterJorneys({ filterStatus: e.target.value })}
          >
            {statusToIcon[0]}
            <Avatar backgroundColor="#E4E6F1" color="#9196AB">
              {tableData.length}
            </Avatar>
          </RadioItem>
          <RadioItem
          // eslint-disable-next-line
            checked={filterStatus == 1}
            value={1}
            onChange={(e) => filterJorneys({ filterStatus: e.target.value })}
          >
            {statusToIcon[1]}
            <Avatar backgroundColor="#E4E6F1" color="#9196AB">
              {tableData.filter((e) => e.status === 1).length}
            </Avatar>
          </RadioItem>
          <RadioItem
          // eslint-disable-next-line
            checked={filterStatus == 2}
            value={2}
            onChange={(e) => filterJorneys({ filterStatus: e.target.value })}
          >
            {statusToIcon[2]}
            <Avatar backgroundColor="#E4E6F1" color="#9196AB">
              {tableData.filter((e) => e.status === 2).length}
            </Avatar>
          </RadioItem>
          <RadioItem
          // eslint-disable-next-line
            checked={filterStatus == 3}
            value={3}
            onChange={(e) => {
              console.log(e);
              filterJorneys({ filterStatus: e.target.value });
            }}
          >
            {statusToIcon[3]}
            <Avatar backgroundColor="#E4E6F1" color="#9196AB">
              {tableData.filter((e) => e.status === 3).length}
            </Avatar>
          </RadioItem>
          <RadioItem
          // eslint-disable-next-line
            checked={filterStatus == 4}
            value={4}
            onChange={(e) => filterJorneys({ filterStatus: e.target.value })}
          >
            {statusToIcon[4]}
            <Avatar backgroundColor="#E4E6F1" color="#9196AB">
              {tableData.filter((e) => e.status === 4).length}
            </Avatar>
          </RadioItem>
          <RadioItem
          // eslint-disable-next-line
            checked={filterStatus == 5}
            value={5}
            onChange={(e) => filterJorneys({ filterStatus: e.target.value })}
          >
            {statusToIcon[5]}
            <Avatar backgroundColor="#E4E6F1" color="#9196AB">
              {tableData.filter((e) => e.status === 5).length}
            </Avatar>
          </RadioItem>
        </RadioGroup>
        <div style={{ marginLeft: "196px", marginTop: "-18px" }}>
          <Table
            titles={["Nome", "Destinatários", "Sucesso", "Status"]}
            data={filteredTableData.map((e) => [
              e.name,
              e.recipients,
              e.success,
              statusToIcon[e.status],
            ])}
          />
        </div>
      </div>
    </main>
  );
};

export { Main };
