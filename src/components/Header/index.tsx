import React, { useState } from "react";
import { useStore } from "../../store";
import {
  Avatar,
  Button,
  Input,
  Logo,
  Modal,
  PlusIcon,
  Tooltip,
} from "d1-react-components";
import * as HeaderContainer from "./styles";

const Header = () => {
  const [newName, setNewName] = useState("");
  const { isModalVisible } = useStore((store) => store.state);
  const { filterJorneys, toggleModal, addJourney } = useStore(
    (store) => store.actions
  );

  return (
    <HeaderContainer.Wraper>
      <HeaderContainer.LeftWraper>
        <Tooltip content="Antônio da Silva">
          <Avatar>A</Avatar>
        </Tooltip>

        <div
          style={{
            padding: "3px 12px 0px 12px",
            backgroundColor: "#FFFFFF",
            width: "fit-content",
            border: "1px solid #EBEEF6",
            borderRadius: "5px",
          }}
        >
          <Logo acme width="66px" />
        </div>
      </HeaderContainer.LeftWraper>
      <HeaderContainer.RightWraper>
        <Input
          search
          onChange={(e) => {
            filterJorneys({ searchName: e.target.value });
          }}
        />
        <Button icon={PlusIcon} onClick={() => toggleModal()}>
          {" "}
          Nova Jornada
        </Button>
        <Modal
          footer={
            <>
              {" "}
              <Button
                onClick={() => {
                  addJourney(newName);
                  toggleModal();
                }}
                ghost
              >
                Continuar
              </Button>{" "}
              <Button
                onClick={() => {
                  toggleModal();
                  setNewName("");
                }}
                ghost
              >
                Cancelar
              </Button>{" "}
            </>
          }
          title="Nova Jornada"
          active={isModalVisible}
          hideModal={() => {
            toggleModal();
            setNewName("");
          }}
        >
          {" "}
          <p style={{ marginBottom: "5px" }}>
            Dê um <strong>nome</strong> para essa Jornada
          </p>
          <Input
            placeholder="Nome"
            onChange={(e) => {
              setNewName(e.target.value);
            }}
          />
          <p style={{ color: "#9196AB", marginTop: "5px" }}>
            Você poderá alterar essa informação depois.
          </p>
        </Modal>
      </HeaderContainer.RightWraper>
    </HeaderContainer.Wraper>
  );
};

export { Header };
