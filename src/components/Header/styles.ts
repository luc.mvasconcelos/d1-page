import styled from 'styled-components'


export const Wraper = styled.header`
position: fixed;
top: 0;
max-height: 90px;
width: 100vw;
display: flex;
align-items: center;
flex-flow:row wrap;
justify-content: space-between;
padding: 27px 0;

`;

export const LeftWraper = styled.header`
display: flex;
align-items: center;
margin-left: 145px;
& > *:last-child{
    margin-left: 15px;
}
`;

export const RightWraper = styled.header`
display: flex;
align-items: center;
float:right;
margin-right: 90px;
& > *:first-child{
    margin-right: 15px;
}
`;