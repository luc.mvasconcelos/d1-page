import React from "react";
import {
  ChartPieIcon,
  CloudIcon,
  DatabaseIcon,
  ExchangeIcon,
  ExternalLinkIcon,
  GemIcon,
  RocketIcon,
  SideNav,
  SidenavFooter,
  SidenavItem,
  SidenavLogo,
  SidenavSeparator,
  SignOutIcon,
  ToolsIcon,
  Tooltip,
  UserFriendIcon,
} from "d1-react-components";

import { BodyWraper } from "./styles";

const Sidenav = () => (
  <nav>
    <SideNav>
      <SidenavLogo>
        <img
          src={
            "https://uploads-ssl.webflow.com/5efcbbdc5b5341ae33f3c9bd/5f872eea2ee9cf8ca58522a2_d1-logo-branca.svg"
          }
          alt="logo"
        />
      </SidenavLogo>
      <SidenavItem>
        <Tooltip
          content={"Análises"}
          color="#9196AB"
          backgroundColor="#EBEEF6"
          animation
        >
          <ChartPieIcon size="21px" />
        </Tooltip>
      </SidenavItem>

      <SidenavItem>
        <Tooltip
          content={"Jornadas"}
          color="#9196AB"
          backgroundColor="#EBEEF6"
          animation
        >
          <RocketIcon size="21px" />
        </Tooltip>
      </SidenavItem>
      <SidenavItem>
        <Tooltip
          content={"Clientes"}
          color="#9196AB"
          backgroundColor="#EBEEF6"
          animation
        >
          <UserFriendIcon size="21px" />
        </Tooltip>
      </SidenavItem>
      <SidenavItem>
        <Tooltip
          content={"CCM Cloud"}
          color="#9196AB"
          backgroundColor="#EBEEF6"
          animation
        >
          <CloudIcon size="21px" />
          <span style={{ marginLeft: "-17px", zIndex: 2 }}>
            <DatabaseIcon size="13px" />
          </span>
        </Tooltip>
      </SidenavItem>

      <SidenavItem>
        <SidenavSeparator />
      </SidenavItem>
      <SidenavItem>
        <Tooltip
          content={"Versão 01"}
          color="#9196AB"
          backgroundColor="#EBEEF6"
          animation
        >
          <ExternalLinkIcon size="21px" />
        </Tooltip>
      </SidenavItem>

      <SidenavFooter>
        <SidenavItem>
          <Tooltip
            content={"Administraçäo"}
            color="#9196AB"
            backgroundColor="#EBEEF6"
            animation
          >
            <GemIcon size="21px" />
          </Tooltip>
        </SidenavItem>
        <SidenavItem>
          <Tooltip
            content={"Help Desk"}
            color="#9196AB"
            backgroundColor="#EBEEF6"
            animation
          >
            <ToolsIcon size="21px" />
          </Tooltip>
        </SidenavItem>
        <SidenavItem>
          <Tooltip
            content={"Trocar Conta"}
            color="#9196AB"
            backgroundColor="#EBEEF6"
            animation
          >
            <ExchangeIcon size="21px" />
          </Tooltip>
        </SidenavItem>
        <SidenavItem>
          <Tooltip
            content={"Sair"}
            color="#9196AB"
            backgroundColor="#EBEEF6"
            animation
          >
            <SignOutIcon size="21px" />
          </Tooltip>
        </SidenavItem>
      </SidenavFooter>
    </SideNav>
  </nav>
);

export { Sidenav, BodyWraper };
