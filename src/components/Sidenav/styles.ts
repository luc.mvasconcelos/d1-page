import styled from "styled-components";

export const BodyWraper = styled.div`
  display: flex;
  main > div:nth-child(2) > div:first-child {
    margin-left: 0;
    display: block;
    & > *:last-child {
      margin-left: 0px;
    }
  }
`;
